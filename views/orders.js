const router = require('express').Router();
const controllers = require('../controllers');

router.route('/orders')
  .get(controllers.OrderController.getAllOrders)
  .post(controllers.OrderController.createOrder);

router.route('/orders/:id')
  .get(controllers.OrderController.getOrder)
  // .put()
  // .delete();

module.exports = router