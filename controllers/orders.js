const models = require('../models');
const ObjectId = require('mongodb').ObjectID;

const getAllOrders = (req, res) => {
  models.Order.find({}, (err, data) => {
    if (err) throw new Error('Can not get all docs')
    res.status(200).json(data);
  })
}

const createOrder = (req, res) => {
  let body = { order: req.body };
  models.Order.create(body, (err, data) => {
    if (err) throw new Error('Can not create doc');
    res.send(`order <b>${data.order}</b> was created with id <b>${data._id}</b>`).end();
  })
}

const getOrder = (req, res) => {
  if (ObjectId.isValid(req.params.id)) {
    models.Order.find({ _id: req.params.id }, (err, data) => {
      if (err) console.log(err);
      if (data[0]) {
        res.status(200).json(data[0]);
        console.log(data);
      } else {
        res.status(200).json({
          order: "There is no such item"
        })
      }
    })
  } else {
    res.status(200).json({
      order: "Enter valid id"
    })
  }
}

// const updateOrder = () => {

// }

// const deleteOrder = () => {

// }

module.exports = {
  getAllOrders,
  createOrder,
  getOrder
}